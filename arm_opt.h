#ifndef ARM_OPT_H
#define ARM_OPT_H

#include <arm_neon.h>

#include <math.h>
#include <stdlib.h>
#ifdef __cplusplus
using namespace std;
#endif

#if defined(__GNUC__) || defined(__clang__)

#pragma push_macro("FORCE_INLINE")
#pragma push_macro("ALIGN_STRUCT")
#define FORCE_INLINE static inline __attribute__((always_inline))
#define ALIGN_STRUCT(x) __attribute__((aligned(x)))

#else

#error "Macro name collisions may happens with unknown compiler"
#ifdef FORCE_INLINE
#undef FORCE_INLINE
#endif

#define FORCE_INLINE static inline
#ifndef ALIGN_STRUCT
#define ALIGN_STRUCT(x) __declspec(align(x))
#endif

#endif

#ifdef __GNUC__
#define LIKELY(x) __builtin_expect(!!(x),1)
#define UNLIKELY(x) __builtin_expect(!!(x),0)
#else
#define LIKELY(x) (x)
#define UNLIKELY (x)
#endif

#define likely LIKELY
#define unlikely UNLIKELY

enum _mm_hint
{
  /* _MM_HINT_ET is _MM_HINT_T with set 3rd bit.  */
  _MM_HINT_ET0 = 7,
  _MM_HINT_ET1 = 6,
  _MM_HINT_T0 = 3,
  _MM_HINT_T1 = 2,
  _MM_HINT_T2 = 1,
  _MM_HINT_NTA = 0    
};

typedef union {
    int8x16_t vect_s8;
    int16x8_t vect_s16;
    int32x4_t vect_s32;
    int64x2_t vect_s64;
    uint8x16_t vect_u8;
    uint16x8_t vect_u16;
    uint32x4_t vect_u32;
    uint64x2_t vect_u64;
} __m128i;

typedef float32x4_t __m128;

typedef float64x2_t __m128d;

// MAIN FUNCTIONS
FORCE_INLINE __m128i _mm_max_epu8(__m128i a, __m128i b)
{
    __m128i res;
    res.vect_u8 = vmaxq_u8(a.vect_u8, b.vect_u8);
    return res;
}

FORCE_INLINE __m128i _mm_max_epi16(__m128i a, __m128i b)
{
	__m128i dst;
	dst.vect_s16 = vmaxq_s16(a.vect_s16, b.vect_s16);
	return dst;
}

FORCE_INLINE __m128i _mm_adds_epu8(__m128i a, __m128i b)
{
    __m128i res;
    res.vect_u8 = vqaddq_u8(a.vect_u8, b.vect_u8);
    return res;
}

FORCE_INLINE __m128i _mm_subs_epu8(__m128i a, __m128i b)
{
	__m128i res;
	res.vect_u8 = vqsubq_u8(a.vect_u8, b.vect_u8);
	return res;
}

FORCE_INLINE __m128i _mm_adds_epi16(__m128i a, __m128i b)
{
    __m128i res;
    res.vect_s16 = vqaddq_s16(a.vect_s16, b.vect_s16);
    return res;
}

FORCE_INLINE __m128i _mm_subs_epu16(__m128i a, __m128i b)
{
	__m128i res;
	res.vect_u16 = vqsubq_u16(a.vect_u16, b.vect_u16);
	return res;
}

FORCE_INLINE __m128i _mm_slli_si128(__m128i a, const int imm8)
{
	__m128i res;
	if (imm8 > 0 && imm8 <= 15) {
		int8x16_t zero = vdupq_n_s8(0);
		__asm__ __volatile__ (
			"ext %0.16b, %1.16b, %2.16b, #%3"
			: "=w"(res.vect_s8)
			: "w"(zero), "w"(a.vect_s8), "i"(16 - imm8)
			: /*No clobbers */);
	} else if (imm8 == 0) {
		res = a;
	} else {
		res.vect_s8 = vdupq_n_s8(0);
	}
	return res;
}

FORCE_INLINE __m128i _mm_srli_si128 (__m128i a, int imm8)
{
    assert(imm8 >=0 && imm8 < 256);
    __m128i res;
    if (likely(imm8 > 0 && imm8 <= 15)) {
        res.vect_s8 = vextq_s8(a.vect_s8, vdupq_n_s8(0), (imm8));
    } else if (imm8 == 0) {
        res = a;
    } else {
        res.vect_s8 = vdupq_n_s8(0);
    }
    return res;
}

FORCE_INLINE __m128i _mm_set1_epi8(char w)
{
    __m128i res;
    res.vect_s8 = vdupq_n_s8(w);
    return res;
}

FORCE_INLINE __m128i _mm_set1_epi16(short a)
{
    __m128i res;
    res.vect_s16 = vdupq_n_s16(a);
    return res;
}

FORCE_INLINE __m128i _mm_set1_epi32(int _i)
{
    __m128i res;
    res.vect_s32 = vdupq_n_s32(_i);
    return res;
}

FORCE_INLINE __m128i _mm_load_si128 (__m128i const* mem_addr)
{
    __m128i res;
    res.vect_s32 = vld1q_s32((const int32_t *)mem_addr);
    return res;
}

FORCE_INLINE void _mm_store_si128 (__m128i* mem_addr, __m128i a)
{
    vst1q_s32((int32_t *)mem_addr, a.vect_s32);
}

FORCE_INLINE int _mm_movemask_epi8(__m128i a)
{
    int res;
    __asm__ __volatile__ (
        "ushr %[a0].16b, %[a0].16b, #7          \n\t"
        "usra %[a0].8h, %[a0].8h, #7            \n\t"
        "usra %[a0].4s, %[a0].4s, #14           \n\t"
        "usra %[a0].2d, %[a0].2d, #28           \n\t"
        "ins %[a0].b[1], %[a0].b[8]             \n\t"
        "umov %w[r], %[a0].h[0]"
        :[r]"=r"(res), [a0]"+w"(a.vect_u8)
        :
        :
    );
    return res;
}

FORCE_INLINE __m128i _mm_cmpeq_epi8 (__m128i a, __m128i b)
{
    __m128i res;
    res.vect_u8 = vceqq_s8(a.vect_s8, b.vect_s8);
    return res;
}

FORCE_INLINE __m128i _mm_cmpgt_epi16(__m128i a, __m128i b)
{
	__m128i dst;
	dst.vect_u16 = vcgtq_s16(a.vect_s16, b.vect_s16);
	return dst;
}

FORCE_INLINE int _mm_extract_epi16(__m128i a, const int imm8)
{
	return a.vect_s16[imm8 & 0x7] & 0xffff;
}

FORCE_INLINE void _mm_prefetch(char const* p, int i)
{
    if(i == 1) {
        __asm__ __volatile__("prfm pldl1keep, %a0 \n\t" ::"p"(p));
    } else if (i == 2) {
        __asm__ __volatile__("prfm pldl2keep, %a0 \n\t" ::"p"(p));
    } else if (i == 3) {
        __asm__ __volatile__("prfm pldl3keep, %a0 \n\t" ::"p"(p));
    }
}

#endif  // END_ARM_OPT_H